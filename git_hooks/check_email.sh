#!/bin/sh
# vim: ts=4 sts=4 sw=4 et
#
# Copyright 2018 Coy <https://gitlab.com/coygo>. All rights reserved.
# Use of this source code is governed by a GPLv3
# license that can be found in the LICENSE file.

set -e

CWD="$(dirname $(realpath $0))"

. ${CWD}/init_colors.inc.sh

check_email() {
    # "|| true" means force to success
    allowemail=$(git config hooks.allowemail || true)

    if [ "$allowemail" != "true" ] && (! echo $GIT_AUTHOR_EMAIL | grep '@noreply.gitlab.com$' >/dev/null)
    then
        echo
        echo ${RED} ERROR ${RESET}
        echo
        echo It seams like you are trying to submit a commit with a real email address \<$GIT_AUTHOR_EMAIL\>, which is not allowed by default. Because it will reveal your personal information. Please try to use a fake email address in form:
        echo
        echo "    <name>@noreply.gitlab.com"
        echo
        echo If you know what you are doing you can disable this check using:
        echo
        echo "    git config hooks.allowemail true"
        echo

        exit 1
    fi
}

check_email
